*********************
Draughts State Viewer
*********************
FabricJS-based, in-browser visualization of draughts game.

============
Installation
============

-----------------
Development setup
-----------------
The quick setup can be performed using `Docker Compose
<https://docs.docker.com/compose/>`_ (recommended):

.. code-block:: bash

    $ docker-compose -f Docker/dev/docker-compose.yml up

This command will install all the dependencies and start HTTP server listening
on 127.0.0.1:9000.

You also need to install static assets used in the project. You can do it by
downloading `assets archive <https://drive.google.com/file/d/0BxteykYB9rF6VTB5Y2k0elZNU1U/view?usp=sharing>`_ and unpacking it to the project's img
directory.

=======
Authors
=======
`Bartosz Litwiniuk <https://gitlab.com/u/Litwiniuk>`_ and `contributors
<https://gitlab.com/radish/draughts-state-viewer/graphs/master/contributors>`_.
