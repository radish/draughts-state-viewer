import 'fabric';
import {IStaticCanvas} from 'fabric';
import {Game} from "./game";
import {Consts} from "./consts";

async function main() {
  let c = document.getElementById('c');
  let canvas: IStaticCanvas = new fabric.StaticCanvas(c);
  let game: Game = new Game();

  // fabric.StaticCanvas overrides style, so we need to reapply it here
  c.style.width = '80vh'
  c.style.height = '80vh'

  await game.main(canvas);
}

main();
