export class Consts {
  margin:number = 76;
  fieldSize:number = 64;
  checkerSize:number = 10;
  playerOnePawnPath:string = 'img/pawn1.svg';
  playerTwoPawnPath:string = 'img/pawn2.svg';
  playerOneQueenPath:string = 'img/queen1.svg';
  playerTwoQueenPath:string = 'img/queen2.svg';
  checkerPath:string = 'img/checker.svg';
  pawnScale:number = 0.48;
  webSockerServerAddress:string = 'ws://localhost:8000';
  configFileName = 'clientConfig.json';
}
