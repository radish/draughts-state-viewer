import {Consts} from './consts';
import {Pawn} from './pawn';
import {Game} from './game';
import {readConfigFile} from './wrappers';

export class WebSocketHelper{

  handleEvent(data): WebSocketData{
    let checker: Pawn[][] = [];
    let game: Game = new Game();
    let result: string = "";
    var message: any = JSON.parse(data);

    if(checker.length == 0)
      checker = game.fillChecker(message.checker);
    return new WebSocketData(checker, message.status);
  }

  prepareMessage(data): string{
    let consts: Consts = new Consts();
    var message = JSON.parse(data);
    let result: string = "";
    for (let i:number = 0; i < consts.checkerSize; i++) {
      for (let j:number = 0; j < consts.checkerSize; j++) {
        if((i+j)%2==0){
        result += message[j][i] + " ";
        }
        else
        {
          result += 0 + " ";
        }
      }
    result += "<br>";
    }
    return result;
  }

  async readConfigFile(path: string)
  {
    let config = JSON.parse(await readConfigFile('clientConfig.json'));
    return new ConfigData(config.webSocketAddress);
  }
}
export class WebSocketData{
  checker: Pawn[][];
  status: number;
  cameraId: number; // so far not used

  constructor(checker: Pawn[][], status: number)
  {
    this.checker = checker;
    this.status = status;
  }

  getMessage(): string
  {
    if(this.status != null)
    {
     if (this.status == 0)
        return "Nothing changed.";
      else if (this.status == 1)
        return "First player win!";
      else if (this.status == 2)
        return "Second player win!";
      else if (this.status == 3)
        return "First player wrong move!";
      else if (this.status == 4)
        return "Second player wrong move!";
      else if(this.status == 5)
        return "First player has forced attack";
      else if(this.status == 6)
        return "Second player has forced attack";
      else
        return "Unrecognize status."
    }
  }
}

export class ConfigData{
  webSocketAddress: string;
  
  constructor(webSocketAddress: string)
  {
    this.webSocketAddress = webSocketAddress;
  }
}
