import {IStaticCanvas} from 'fabric';
import {Consts} from './consts';
import {Pawn} from './pawn';
import {PawnMove} from './pawn';
import {Draw} from './draw';
import {WebSocketHelper} from './webSocket';
import {WebSocketData} from './webSocket';
import {ConfigData} from './webSocket';

export class Game {

  fillChecker(stateTable: Number[][]): Pawn[][]{
    let consts = new Consts();
    let checker: Pawn[][] = [];
    let id: number = 0;
    for (let i:number = 0; i < consts.checkerSize; i++) {
      checker[i] = [];
      for (let j:number = 0; j < consts.checkerSize; j++) {
        if (stateTable[i][j] == 1)
        {
          checker[i][j] = new Pawn(id, i, j, 1, false);
          id++;
        }
        else if (stateTable[i][j] == 2)
        {
          checker[i][j] = new Pawn(id, i, j, 2, false);
          id++;
        }
        else if (stateTable[i][j] == 3)
        {
          checker[i][j] = new Pawn(id, i, j, 1, true);
          id++;
        }
        else if (stateTable[i][j] == 4)
        {
          checker[i][j] = new Pawn(id, i, j, 2, true);
          id++;
        }
        else
        {
          checker[i][j] = null;
        }
      }
    }
    return checker
  }

  compareChecker(checker: Pawn[][], nextChecker: Pawn[][])
  {
    let pawnToMove: PawnMove = null;
    let draw = new Draw();
    let consts = new Consts();
    pawnToMove = this.findPawnWhichMoved(checker,nextChecker);
    let pawnsWhichDisappeared: Pawn[] = this.findPawnsWhichDissappeared(checker, nextChecker);
  }

  removeFromChecker(checker: Pawn[][], pawnsToRemove: Pawn[]): Pawn[][]
  {
    let consts = new Consts();
    for (let i: number = 0; i < pawnsToRemove.length; i++) {
      for (let j: number = 0; j < consts.checkerSize; j++) {
        for (let k: number = 0; k < consts.checkerSize; k++) {
          if(checker[j][k] != null && checker[j][k].id == pawnsToRemove[i].id)
            checker[j][k] = null;
        }
      }
    }
    return checker;
  }

  movePawnOnChecker(checker: Pawn[][], movePawn: PawnMove)
  {
    if(movePawn.pawnWhichMoved != null)
    {
      let outdatedX: number = movePawn.pawnWhichMoved.positionX;
      let outdatedY: number = movePawn.pawnWhichMoved.positionY;

      if(checker[outdatedX][outdatedY] != null &&
        checker[outdatedX][outdatedY].id == movePawn.pawnWhichMoved.id)
            checker[outdatedX][outdatedY] = null;

      movePawn.pawnWhichMoved.movePawn(movePawn.newPositionX,movePawn.newPositionY);
      checker[movePawn.newPositionX][movePawn.newPositionY] = movePawn.pawnWhichMoved;
    }
  }

  findPawnWhichMoved(checker: Pawn[][], nextChecker: Pawn[][]): PawnMove
  {
    let consts = new Consts();
    let pawnMove = new PawnMove();
    for (let i: number = 0; i < consts.checkerSize; i++) {
      for (let j: number = 0; j < consts.checkerSize; j++) {
        if(checker[i][j] != null && nextChecker[i][j] == null)
        {
          pawnMove.pawnWhichMoved = checker[i][j];
        }
        else if(checker[i][j] == null && nextChecker[i][j] != null)
        {
          pawnMove.newPositionX = i;
          pawnMove.newPositionY = j;
        }
        else if(pawnMove.pawnWhichMoved != null && pawnMove.newPositionX != null && pawnMove.newPositionY != null)
        {
          if(pawnMove.pawnWhichMoved.isQueen == false && nextChecker[pawnMove.newPositionX][pawnMove.newPositionY].isQueen)
          {
            pawnMove.isFigureChanging = true;
          }
          return pawnMove;
        }
      }
    }
    return pawnMove;
  }

  findPawnsWhichDissappeared(checker:Pawn[][], nextChecker: Pawn[][]): Pawn[]
  {
    let consts = new Consts();
    let pawnsWhichDisappeared: Pawn[] = [];
    let firstPlayerPawnsCountBeforeMove: number = 0;
    let firstPlayerPawnsCountAfterMove: number = 0;
    let secondPlayerPawnsCountBeforeMove: number = 0;
    let secondPlayerPawnsCountAfterMove: number = 0;

     for (let i:number = 0; i < consts.checkerSize; i++) {
      for (let j:number = 0; j < consts.checkerSize; j++) {
        if(checker[i][j] != null)
        {
          if(checker[i][j].playerBelonging == 1)
            firstPlayerPawnsCountBeforeMove++;
          else if(checker[i][j].playerBelonging ==2)
            secondPlayerPawnsCountBeforeMove++;
        }
        if(nextChecker[i][j] != null)
        {
          if(nextChecker[i][j].playerBelonging==1)
            firstPlayerPawnsCountAfterMove++;
          else if(nextChecker[i][j].playerBelonging ==2)
            secondPlayerPawnsCountAfterMove++;
        }
      }
     }

    if(firstPlayerPawnsCountAfterMove < firstPlayerPawnsCountBeforeMove)
    {
      for (let i:number = 0; i < consts.checkerSize; i++) {
        for (let j:number = 0; j < consts.checkerSize; j++) {
          if(checker[i][j]!=null && ((checker[i][j].playerBelonging == 1 && nextChecker[i][j]==null) ||
            (checker[i][j].playerBelonging == 0 && nextChecker[i][j].playerBelonging==1)))
            pawnsWhichDisappeared[pawnsWhichDisappeared.length] = checker[i][j];
        }
      }
    }

    else if(secondPlayerPawnsCountAfterMove < secondPlayerPawnsCountBeforeMove)
    {
      for (let i:number = 0; i < consts.checkerSize; i++) {
        for (let j:number = 0; j < consts.checkerSize; j++) {
          if(checker[i][j]!=null && checker[i][j].playerBelonging == 2 && nextChecker[i][j]==null)
            pawnsWhichDisappeared[pawnsWhichDisappeared.length] = checker[i][j];
        }
      }
    }

    return pawnsWhichDisappeared;
  }
  
  handleDataStatus(canvas: IStaticCanvas, webSocketData: WebSocketData, lastStatus: number, checker:Pawn[][]){
      let draw: Draw = new Draw();
      console.log('last status: ' + lastStatus);
      console.log('webSocketDataStatus: ' + webSocketData.status);
      if(webSocketData.status == lastStatus)
      {
        return;
      }
      else if(webSocketData.status == 0)
      {
        checker = [];
      }  
      else if (webSocketData.status == null && lastStatus != 0)
      {
        draw.removeMessage(canvas);
      }
      else if (lastStatus != null && webSocketData.status != null && lastStatus != webSocketData.status)
      {
        draw.removeMessage(canvas)
        draw.showMessage(canvas, webSocketData.getMessage());  
      }
      else if (lastStatus == null && webSocketData.status != null)
      {
        draw.showMessage(canvas, webSocketData.getMessage());       
      }
  }

  async main(canvas: IStaticCanvas) {
    let draw: Draw = new Draw();
    let game : Game = new Game();
    let consts: Consts = new Consts();
    let checker: Pawn[][] = [];
    let nextChecker: Pawn[][] = [];
    let webSocketData: WebSocketData = null;
    let lastStatus: number = null;
    let webSocketHelper: WebSocketHelper = new WebSocketHelper();

    let configData: ConfigData = await webSocketHelper.readConfigFile(consts.configFileName);
    let webSocket: WebSocket = new WebSocket(configData.webSocketAddress); 
    webSocket.onmessage = async function (event) {

      // document.getElementById("stateTable").innerHTML = webSocketHelper.prepareMessage(event.data);
      if (checker.length == 0)
      {
        webSocketData = webSocketHelper.handleEvent(event.data);
        checker = webSocketData.checker;
        await draw.drawState(canvas, checker);
      }
      else
      {
        webSocketData = webSocketHelper.handleEvent(event.data);
        if(webSocketData.checker !== null)
        {
          nextChecker = webSocketData.checker;
          let pawnsToRemove: Pawn[] = game.findPawnsWhichDissappeared(checker, nextChecker);

          let pawnRemovalTask = null;
          if(pawnsToRemove != null && pawnsToRemove.length != 0)
          {
            pawnRemovalTask = draw.removePawns(canvas,pawnsToRemove);
            checker = game.removeFromChecker(checker, pawnsToRemove);
          }
          let pawnMove = game.findPawnWhichMoved(checker, nextChecker);
          if(pawnMove != null)
          {
            game.movePawnOnChecker(checker, pawnMove);
            await draw.movePawn(canvas, pawnMove);
          }

          await pawnRemovalTask;
        }          
        game.handleDataStatus(canvas, webSocketData, lastStatus, checker);
        lastStatus = webSocketData.status;     
      }
    }
  }
}
