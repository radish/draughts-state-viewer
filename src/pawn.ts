export class Pawn{
  id : number;
  positionX: number;
  positionY: number;
  playerBelonging: number;
  isQueen: boolean = false;

  constructor(id: number, positionX: number, positionY: number, playerBelonging: number, isQueen: boolean){
    this.id = id;
    this.positionX = positionX;
    this.positionY = positionY;
    this.playerBelonging = playerBelonging;
    this.isQueen = isQueen;
  }

  movePawn(positionX: number, positionY: number){
    this.positionX = positionX;
    this.positionY = positionY;
  }
}

export class PawnMove{
  pawnWhichMoved: Pawn;
  newPositionX: number;
  newPositionY: number;
  isFigureChanging: boolean = false;
}
