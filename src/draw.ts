import 'fabric';
import {IStaticCanvas, IImage} from 'fabric';
import {Consts} from './consts';
import {Pawn} from './pawn';
import {PawnMove} from './pawn';
import {imageFromURL, animate} from './wrappers';

export class Draw {
  messageBoxId = -1;
  messageTextId = -2;
  async drawState(canvas: IStaticCanvas, checker: Pawn[][]) {
    let consts = new Consts();

    for (var i = 0; i < consts.checkerSize; i++) {
      for (var j = 0; j < consts.checkerSize; j++) {
        let width: number = consts.margin-1 + (i * consts.fieldSize);
        let height:number = consts.margin + (j * consts.fieldSize);
        let img;

        if(checker[i][j] != null) {
          if (checker[i][j].playerBelonging == 1 && checker[i][j].isQueen == false) {
            img = await imageFromURL(consts.playerOnePawnPath, {
              id: checker[i][j].id,
              left: width,
              top: height,
              scaleX: consts.pawnScale,
              scaleY: consts.pawnScale,
            });
          }
          else if (checker[i][j].playerBelonging == 2 && checker[i][j].isQueen == false) {
            img = await imageFromURL(consts.playerTwoPawnPath, {
              id: checker[i][j].id,
              left: width,
              top: height,
              scaleX: consts.pawnScale,
              scaleY: consts.pawnScale,
            });
          }
          else if (checker[i][j].playerBelonging == 1 && checker[i][j].isQueen == true) {
            img = await imageFromURL(consts.playerOneQueenPath, {
              id: checker[i][j].id,
              left: width,
              top: height,
              scaleX: consts.pawnScale,
              scaleY: consts.pawnScale,
            });
          }
          else if (checker[i][j].playerBelonging == 2 && checker[i][j].isQueen == true) {
            img = await imageFromURL(consts.playerTwoQueenPath, {
              id: checker[i][j].id,
              left: width,
              top: height,
              scaleX: consts.pawnScale,
              scaleY: consts.pawnScale,
            });
          }
        }
        if(img != null)
          canvas.add(img).renderAll();
      }
    }
  }

  async removePawns(canvas: IStaticCanvas, pawns: Pawn[]){
    for(let i: number = 0; i < pawns.length; i++)
    {
      var object = this.canvasGetObjectById(canvas,pawns[i].id);
      await animate(object, 'opacity', 0, {
        onChange: canvas.renderAll.bind(canvas),
        duration: 600
      });
      //canvas.remove(object);
    }
  }

  async movePawn(canvas: IStaticCanvas, pawnMove: PawnMove)
  {
    if(pawnMove.pawnWhichMoved != null)
    {
      let consts = new Consts();
      let width: number = consts.margin + (pawnMove.newPositionX * consts.fieldSize);
      let height: number = consts.margin + (pawnMove.newPositionY * consts.fieldSize);

      var object = this.canvasGetObjectById(canvas, pawnMove.pawnWhichMoved.id);

      let left_move = animate(object, 'left', width, {
        duration: 800,
        onChange: canvas.renderAll.bind(canvas),
        easing: fabric.util.ease['easeInQuad']
      });

      let top_move = animate(object, 'top', height, {
        duration: 800,
        onChange: canvas.renderAll.bind(canvas),
        easing: fabric.util.ease['easeInQuad']
      });

      await left_move;
      await top_move;

      if(pawnMove.isFigureChanging)
      {
        let object = this.canvasGetObjectById(canvas, pawnMove.pawnWhichMoved.id);

        let img = new Image();
        img.onload = () => {
          object.setElement(img, canvas.renderAll.bind(canvas), null);
        };
        if(pawnMove.pawnWhichMoved.playerBelonging == 1) {
          img.src = consts.playerOneQueenPath;
        }
        else if(pawnMove.pawnWhichMoved.playerBelonging == 2) {
          img.src = consts.playerTwoQueenPath;
        }
      }
    }
  }

  canvasGetObjectById(canvas: IStaticCanvas, id: number): IImage
  {
    let objects = canvas.getObjects() as Array<IImage>;
    for (let object of objects) {
      if (object.id != null && object.id === id) {
        return object;
      }
    }
  }
  
  showMessage(canvas: IStaticCanvas, message: string)
  {
    let messageHeight:number = 0.2 * canvas.height;
    let messageTop: number = (canvas.height - messageHeight) / 2;
    
    let rect = new fabric.Rect({
        left: 0,
        top: messageTop,
        originX: 'left',
        originY: 'top',
        width: canvas.width,
        height: messageHeight,
        angle: 0,
        fill: 'rgba(56,122,140,0.5)',
        transparentCorners: false,
        id: this.messageBoxId 
    });
    canvas.add(rect);
    
    let textMargin = 60;
    
    let text = new fabric.Text(message, { 
      left: rect.left + textMargin,
      top: rect.top + textMargin, 
      fill: 'white',
      fontFamily: 'Raleway',
      id: this.messageTextId
    });
    canvas.add(text);
    
    canvas.renderAll(); 
  }
  
  async removeMessage(canvas: IStaticCanvas)
  {
    var object = this.canvasGetObjectById(canvas,this.messageTextId);
    let text = animate(object, 'opacity', 0, {
        onChange: canvas.renderAll.bind(canvas),
        duration: 600
      });
    
    
    object = this.canvasGetObjectById(canvas,this.messageBoxId);
    let messageBox = animate(object, 'opacity', 0, {
        onChange: canvas.renderAll.bind(canvas),
        duration: 600
      });
      
    await text;
    await messageBox;
    
    await canvas.remove(this.canvasGetObjectById(canvas, this.messageBoxId));
    await canvas.remove(this.canvasGetObjectById(canvas, this.messageTextId));
  }
}
