import 'fabric';
import {IImage, IObject, IObjectOptions, IAnimationOptions} from 'fabric';
import {ConfigData} from './webSocket';

export function imageFromURL(url: string,
                             objObjects?: IObjectOptions): Promise<IImage> {
  return new Promise(function(resolve) {
    fabric.Image.fromURL(url, function (img) {
      resolve(img);
    }, objObjects);
  });
}

export function animate(object: IObject, property: string, value: number|string,
                        options?: IAnimationOptions): Promise<{}> {
  return new Promise(function(resolve) {
    if (options == null) {
      options = {
        onComplete: () => {
          resolve()
        }
      }
    }
    else {
      options.onComplete = () => {
        resolve()
      };
    }

    object.animate(property, value, options);
  });
}

export function readConfigFile(path: string) {
  return new Promise(function(resolve, reject) {
      let req = new XMLHttpRequest();
      req.open('GET', path);

      req.onload = function() {
        if (req.status == 200) {
          resolve(req.responseText);
        }
        else {
          reject(Error(req.statusText));
        }
      };

      req.onerror = function() {
        reject(Error("Network Error"));
      };

      req.send();
    })
}
