'use strict';
let gulp = require('gulp');
let gulp_jspm = require('gulp-jspm');
let sourcemaps = require('gulp-sourcemaps');

gulp.task('bundle', ['build'], function(){
    return gulp.src('src/main.js')
    .pipe(sourcemaps.init())
    .pipe(gulp_jspm())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/'));
});
