'use strict';
let gulp = require('gulp');
let paths = require('../paths');
let browserSync = require('browser-sync');

gulp.task('watch', ['serve'], function() {
  gulp.watch(paths.html, ['build-html', browserSync.reload]);
  gulp.watch(paths.css, ['build-css', browserSync.reload]);
  gulp.watch(paths.source, ['build-js', browserSync.reload]);
});
