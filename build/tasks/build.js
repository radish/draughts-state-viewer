'use strict';
let gulp = require('gulp');
let changed = require('gulp-changed');
let runSequence = require('run-sequence');
let ts = require('gulp-typescript');
let sourcemaps = require('gulp-sourcemaps');
let tsProject = ts.createProject('tsconfig.json');
let paths = require('../paths');
let clean = require('./clean');

gulp.task('build-html', function() {
  return gulp.src(paths.html)
    .pipe(changed(paths.output, {extension: '.html'}))
    .pipe(gulp.dest(paths.output));
});

gulp.task('build-css', function() {
  return gulp.src(paths.css)
    .pipe(changed(paths.output, {extension: '.css'}))
    .pipe(gulp.dest(paths.output));
});

gulp.task('build-js', function() {
    return tsProject.src()
      .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(ts(tsProject))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(paths.output));
});

gulp.task('build', function(callback) {
  return runSequence(
    'clean',
    ['build-html', 'build-css', 'build-js'],
    callback
  );
});
