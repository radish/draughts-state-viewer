'use strict';
let gulp = require('gulp');
let vinylPaths = require('vinyl-paths');
let del = require('del');
let paths = require('../paths');

gulp.task('clean', function() {
  return gulp.src([paths.output])
    .pipe(vinylPaths(del));
});
